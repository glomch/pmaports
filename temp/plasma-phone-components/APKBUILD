# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-phone-components
pkgver=5.18.80_git20200219
pkgrel=1
_commit="b5783fedf5b74399a0390b5d9849acbe9fc70eb8"
pkgdesc="Modules providing phone functionality for Plasma"
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="plasma-nano qt5-qtquickcontrols2 plasma-workspace dbus-x11 kactivities plasma-pa plasma-nm libqofono breeze-icons plasma-settings telepathy-ofono telephony-service"
makedepends="extra-cmake-modules kpeople-dev qt5-qtdeclarative-dev kactivities-dev plasma-framework-dev kservice-dev kdeclarative-dev ki18n-dev kio-dev kcoreaddons-dev kconfig-dev kbookmarks-dev kwidgetsaddons-dev kcompletion-dev kitemviews-dev kjobwidgets-dev solid-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev kpackage-dev kwindowsystem-dev kdbusaddons-dev knotifications-dev kwayland-dev telepathy-qt-dev libphonenumber-dev"
source="
	$pkgname-$_commit.tar.gz::https://invent.kde.org/kde/plasma-phone-components/-/archive/$_commit/plasma-phone-components-$_commit.tar.gz
	set-postmarketos-wallpaper.patch
	"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B "$builddir"/build \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make -C build
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" make -C build install
}
sha512sums="8cba6d2eb2c02f4c04f47d0678f12466f37aa869459b2be50fc9cdcb70278576bf33f4f127583826b402438c8336c2ab32caec4845757a13fd4fe3006d13b09a  plasma-phone-components-b5783fedf5b74399a0390b5d9849acbe9fc70eb8.tar.gz
5853e72077c356f8347ea1c8503d5e301505acaa39ccf2fa105abe054a3063e735f5bcb2db1c1357f53032714599bfbf687fff2cbe21a1743930b946900ff7cf  set-postmarketos-wallpaper.patch"
